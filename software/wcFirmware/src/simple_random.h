/*------------------------------------------------------------------------------------------------------------------------------------------------*//**
 * @file simple_random.h
 *
 *  This file implements a very simple 8bit random number generator.
 *
 *  \details
 *     Main Prinziple: http://en.wikipedia.org/wiki/Linear_congruential_generator\n
 *     This is based on Simon K�ppers code posted here: \n
 *         http://www.mikrocontroller.net/topic/112114#998347 \n
 *     The postet version has a bug in the multiplikator that makes it unusable.
 *     This is fixed by using 17 as multiplikator.
 *     With the values 17/37 it has a peridicity of 256.
 *
 *
 * \version $Id: $
 *
 * \author Copyright (c) 2013 Vlad Tepesch
 *
 * \remarks
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 */
 /*-----------------------------------------------------------------------------------------------------------------------------------------------*/




#ifndef _WC_SIMPLE_RNG_H_
#define _WC_SIMPLE_RNG_H_

#include <stdint.h>

#ifdef __cplusplus
extern  "C"
{
#endif

/** initializes the RNG with the given value*/
void simpleRand_setSeed(uint8_t i_seed);

/** returns a pseudo random byte */
uint8_t simpleRand_get();



#ifdef __cplusplus
}
#endif


#endif //_WC_SIMPLE_RNG_H_
