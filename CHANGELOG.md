
# WordClock Change Log

## V 0.13a
* fixt falsche Zeitanzeige von HH:20 Uhr
* fügt ein paar Zeitanzeigevarianten hinzu, aus denen der Narren-Modus auswählt
* Änderungen für Übersetzbarkeit mit MS-Compiler

## V 0.13 (428)
* gefixt BUG012_031 (Fehler in der Logik des Ambilight bei der automatischen Deaktivierung)
* PCB Version 2.0 hinzugefügt
* verbessertes Fading durch 4kHz-PWM
* verbesserter Demo-Modus (Umschaltung zwischen altem Modus und Aktivierung aller Segmente)
* flexiblere Implementierung display_wc_ger3
* Narren-Modus

## V 0.12 (416)
* Schwaben-Modus (ossi + 20 vor/nach)
* Auto-Aus-Animation konfigurierbar mit Vorschau
* Deaktivierbares "IT IS" für englische Front
* Demo modus kann über Demo-Modus-Taste wieder verlassen werden
* PWM modus für bessere Darstellung dunklerer Farben geändert (siehe BUG011_027)
* PWM Stufen für linearere Erscheinung geändert
* optisches Feedback bei "Helligkeit übernehmen" hinzugefügt (kurzzeitige Displayabschaltung)
* gefixt BUG011_025 (Absturz bei Helligkeitskalibrierung)
* gefixt BUG011_026 (Ambilight wird beim automatischen Ausschalten nicht (in jedem Fall) mit ausgeschaltet)
* gefixt BUG011_027 (Niedrige Farbqualität bei niedrigen Helligkeiten)
* gefixt BUG011_028 Fehler bei Zeitüberblendung - Auto-off-Animation ist nun unabhängig von Zeitüberblendung
* verbessert BUG011_29 Flackern be Zeitüberblendung reduziert (immer noch nicht perfekt!)
* gefixt BUG011_030 (SW Absturz wenn "flasche" Taste im Aus-Zustand gedrückt wurde (Statemachine wird nun immer geupdated))
* aktualisiertes Handbuch
  * neue Features
  * "2.1 übersicht der möglichen Kommandos" enthält nun Platz für Benutzer zum Eintragen seiner Tastennamen

## V 0.11 (354)
* gefixt BUG08_002 (Helligkeitskontrolle funktioniert nicht)
* Kalibrierung der automatischen Helligkeitsanpassung
  * neues IR-Kommando
  * neuer Eeprom-Parameter
  * neue Loggingoption
* an Code-Konfig anpassendes latex-basiertes Handbuch (siehe readme)
* IOs per IR ein/abschaltbar
  * neue IR-Kommandos
  * neue Eeprom-Parameter
* gefixt BUG09_022 (Automatische Abschaltung) geänderte Logik (-> Handbuch)
* Autosave optional (user.h USER_AUTOSAVE)
* Eeprom-parameter-Sicherung bei An/Aus IR-cmd
* signalisierung im Auto-Aus-Zustand
* gefixt BUG010_023 (schnelle Modus-Umschaltung verhindert Fading)
* Schrittweite der Auschaltzeiten als define

## V 0.10 (339)
* abschaltbares "ES IST" via Sprach-Wahl-Taste (beide deutsche Front designs) (übernommen von wichtel - aber Reihenfolge geändert -> Handbuch)
* **Bootloader Support:** Wenn 'R' über die UART empfangen wird, wird ein Watchdog-Reset ausgelöst. Der Watchdog wird direkt nach Systemstart deaktiviert.
* Indikator für Zeiteingabe definiert -> blinkendes 'Uhr', wenn keine Minutenwörter aktiv
* Helligkeit für Nachtstunden bei Zeiteingabe reduziert.
* Sicherung des Hauptmodus (Einfarb-, Farbwechsel- und Pulse-Modus) und des aktiven Farbprofils im EEPROM
* BAUD_ERROR Makro aktualisiert
* atmega88 Konfiguration entfernt
* DCF77 geändert, zwei erfolgreich empfangene Frames vor Zeitübernahme notwendig
* IRMP-Version 1.7.2: Bugfix: Timeout vor NEC repetition frames um "Geister-Kommandos" zu vermeiden einige weniger wichtige Protokolle hinzugefügt
* gefixt BUG09_018 (Zeitupdate während Zeiteingabe beinträchtigt Anzeige)
* gefixt BUG09_019 (gemeldet von Wichtel) ("Gesiter-Kommandos" siehe neue IRMP-Version)
* gefixt BUG09_020 (gemeldet von Roman) DCF-Initialisierung <=6 anstatt <=7
* gefixt BUG09_021 (Ambilight in SW an OUTG2 anstatt OUTG1)
* gefixt Fehler der in Mono-Color-Variante: zurückschalten in Normal-Mode forciert kein Display-Update
* complete (mixed english/german)

## V 0.13 (428)
* manual: updateddemo mode and added jester mode to mode descriptions added phillip to authors
* reworked internal implementation of display_wc_ger3 to make adding variants very easy also added jester mode
* improved demo mode: reacts on Up/Down keys and switches between old style (one segment lit up at a time) and fast multiplexing of 4 segments (1 on each driver) st a time so they all appear as switched on
* 4kHz-PWM & New display fading
* New: PCB Version 2.09
* fixed bug that after an auto-off auto-on cycle a manually switched off ambilight will be activeated again, without users interaction (thanks to bernd_m)

## V 0.12 (416)
* swabian idiom selectable
* made auto-off-animation configurable with preview
* deactivatable it is in english front
* demo mode leavable with additional demo mode key press
* changed PWM mode, now also dark colors are displayed vividly (see BUG011_027)
* improved the visual linearity of the PWM-steps
* added an acknowledge (display 500ms off), when overtaking a new brightness
* fixed BUG011_025 (crash on brightness calibration)
* fixed BUG011_026 (ambilight will not be switched off on AutoOff with activated Animation (merged from edimahler))
* fixed BUG011_027 (low  color quality on low brightenss)
* fixed BUG011_028 bug in fade time, now auto-off-anim-fade-time is independent of time-change-fade-time
* improved BUG011_29 (flickering at time-change-fades (sometimes still not perfect!))
* fixed BUG011_030 (SW crashed when pressing a "wrong" button while clock was off (now state machine always updated)
* updated manual
  * to new features
  * remote command table with extra space for user key names

## V 0.11 (354)
* fixed BUG08_002 (brighness control does not work)
* added calibration of auto brightness adaption
  * new IR-Command
  * new eeprom params
  * new logging option
* added self configuring LaTeX user manual (see readme)
* made other GPOs IR-controllable (taken from wichtels code)
  * new eeprom params
  * new IR-Commands
* fixed BUG09_022 (auto switch off) changed on/off logic
* made auto save optional (user.h USER_AUTOSAVE)
* added eeprom parameter write back at On/Off IR-cmd
* added auto off animation
* fixed bug BUG010_023 (fast display mode switching stops fading)
* moved step size of on/off time as constant to header


## V 0.10 (339)
* deactivatable "ES IST" via display mode key (both german front designs) (merge from wichtel - but changed order)
* **Bootloader Support:** If 'R' was received via UART, the Watchdog will be enabled to reset the device. The wdt is deactivated right after system start up.
* added a Set-Time-Indicator (hour for word clock) to signalize Set-Time-Mode if no minute word is active
* reduced brightness for night hours in set time mode
* save main mode (normal, fade, pulse) and active color preset
* change BAUD_ERROR macro
* removed atmega88 configuration
* DCF77 changed, so that two successful received frames are now needed, befor the dcf time will be taken over
* IRMP-Version 1.7.2: bugfix: added timeout for NEC repetition frames to avoid "ghost commands" added some (less relevant) protocols
* fixed BUG09_018 (while entering time a timeupdate destroys current display)
* fixes BUG09_019 (reported by Wichtel) (new IRMP-Version)
* fixed BUG09_020 (reported by Roman) DCF-Initialisierung <=6 instead <=7
* fixed BUG09_021 (Ambilight connected to OUTG2 in SW instead of OUTG1)
* fixed bug that occurs in mono color if switched back to normal mode
  * no update of display was done

## V 0.9 Patch 5
* fixed BUG09_017 (set second to 0, when new DCF77 time will take over - prevent a minute jump)
V 0.9 Patch 4 (312)
* fixed BUG09_015 (after enter OnOff-Time no further action is possible)
* fixed BUG09_016 (last Ir-Command is ignored in training)
V 0.9 Patch 3 (311)
* fixed BUG09_009 (crashes after IR-Kommands)
* fixed BUG09_011 (training bug)
* fixed BUG09_012 (casing on include usermodes.c)
* fixed BUG09_013 (1:00 - 1:04 and 1:05-1:09 's')
* fixed BUG09_014 (brightness control does not work after setting time)
* fixed wrong command handler in display_x-header
* fixed default values for color profiles
* extracted inits of states from user_init to own routine in usermodes.c

## V 0.9 Patch 2 (302)
* fixt BUG09_010 (EIN <-> EINS)

## V 0.9 Patch 1 (300)
* fixt BUG09_008 (falsche Zeitanzeige)

## V 0.9  (298)
* Unterstützung für neue (3 sprachige) deutsche Front
* Unterstützung für TIX-Clock
* kurze Anzeige von Submodi (Farbprofilauswahl, Sprachvariante)
* Helligkeits-Offset wird abgespeichert
* 24h Zeiteingabe (8-20Uhr: hell, 20-8Uhr: dunkel)
* Standardeeprom-Werte im Flash
* Ein/Aus-Schalt-Zeiten
* Pulsierender Modus
* neue IRMP-Version
